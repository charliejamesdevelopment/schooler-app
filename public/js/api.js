if(!cookieExists("authentication_token")) {
  swal({
    title: 'Oops!',
    type: 'error',
    html:
      "Your session has expired, please login!",
    confirmButtonText: 'Login',
    showCancelButton: false,
    allowOutsideClick: false
  }).then(function () {
    window.location = "/main/logout";
  }).catch(swal.noop);
}

$(document).ready(function() {
  getData(getUrl() + '/api/get_group_by_uuid/'+account_uuid, function(group) {
    if(group == 1) {
      $('.avatar-title').text("Something went wrong!");
    } else {
      $('.avatar-title').text(group.group_name);
    }
  });
});

$('#logout').click(function() {
  eraseCookie('authentication_token');
});

function openBooking() {
  getJsonFromTeachers(getUrl()+"/api/get_teachers", function(teacherJSON) {
    var teacher_data = {};
    var teacher_data_avaliable_times = {};
    var times_arr = convertTimesToJson(times);
    var types_arr = convertTypesToJson(types);
    data = JSON.parse(data);
        swal.setDefaults({
          input: 'text',
          confirmButtonText: 'Next &rarr;',
          showCancelButton: true,
          animation: false,
          progressSteps: ['1', '2', '3', '4', '5', '6'],
          allowOutsideClick: false
        })

        teacher_obj = convertJsonToSweetAlert(teacherJSON);
        child_obj = convertJsonToSweetAlert(data);
        var steps = [
          {
            title: 'Select Child',
            input: 'select',
            type: 'question',
            inputOptions: child_obj,
            inputPlaceholder: 'Select Child...',
            showCancelButton: true,
            inputValidator: function (value) {
              return new Promise(function (resolve, reject) {
                if (value != '') {
                  resolve()
                } else {
                  reject('Make sure to select a child!')
                }
              })
            }
          },
          {
            title: 'Select Teacher',
            type: 'question',
            input: 'select',
            inputOptions: teacher_obj,
            inputPlaceholder: 'Select Teacher...',
            showCancelButton: true,
            inputValidator: function (value) {
              return new Promise(function (resolve, reject) {
                if (value != '') {
                  resolve();
                } else {
                  reject('Make sure to select a teacher!')
                }
              })
            }
          },
          {
            title: 'Date of appointment',
            type: 'question',
            text: 'Enter the date you would like to meet on.',
            html:
              '<input id="datepicker" class="form-control" placeholder="Click to select a date..." type="text">',
            input: null,
            preConfirm: function () {
              return new Promise(function (resolve, reject) {
                if($('#datepicker').val() == "") {
                  reject('Make sure to select a date!')
                } else {
                  resolve([
                    $('#datepicker').val(),
                  ])
                }
              })
            },
            onOpen: function () {
              $('#datepicker').datepicker();
            }
          },
          {
            title: 'What time do you want your appointment?',
            type: 'question',
            input: 'select',
            inputOptions: times_arr,
            inputPlaceholder: 'Select Time...',
            showCancelButton: true,
            inputValidator: function (value) {
              return new Promise(function (resolve, reject) {
                if (value != '') {
                  resolve()
                } else {
                  reject('Make sure to select a time!')
                }
              })
            }
          },
          {
            title: "Details of Appointment",
            type: 'question',
            text: 'Please tell us why you want an appointment.',
            input: "textarea",
            inputValidator: function (value) {
              child_id = value;
              return new Promise(function (resolve, reject) {
                if (value != '') {
                  resolve()
                } else {
                  reject('Please tell us why you are requesting an appointment!')
                }
              })
            }
          },
          {
            title: 'Type of Appointment',
            input: 'select',
            type: 'question',
            inputOptions: types_arr,
            inputPlaceholder: 'Select Type of Appointment...',
            showCancelButton: true,
            inputValidator: function (value) {
              return new Promise(function (resolve, reject) {
                if (value != '') {
                  resolve()
                } else {
                  reject('You need to select a type of appointment :)')
                }
              })
            }
          }
        ]

        swal.queue(steps).then(function (result) {
          swal.resetDefaults()

          swal({
            title: 'Final Confirmation!',
            type: 'info',
            html:
              'Are you sure you want to request an appointment?',
            confirmButtonText: 'Request!',
            showCancelButton: true,
            showLoaderOnConfirm: true,
            allowOutsideClick: false,
            preConfirm: function (email) {
              return new Promise(function (resolve, reject) {
                setTimeout(function() {
                  resolve();
                }, 3000)
              })
            }
          }).then(function () {
            var url = getUrl() + "/api/check_date_time_avaliable/" + result[2][0] + "/" + result[3] + "/" + result[1];
            getData(url, function(data) {
              if(data.response == 1){
                swal({
                  title: 'Oops!',
                  type: 'error',
                  html:
                    data.message,
                  confirmButtonText: 'Lets try again!',
                  showCancelButton: true,
                  allowOutsideClick: false
                }).then(function () {
                  openBooking();
                }).catch(swal.noop);
              } else {
                date = result[2][0];
                var send = {"parent_id" : parent_id, "student_id": result[0], "teacher_id": result[1], "date": date, "time": result[3], "details": result[4], "type":result[5]}
                createAppointmentJson(getUrl() + "/api/create_appointment", send, function(res) {
                  if(res.response == 0) {
                    swal({
                      title: 'Requested!',
                      type: 'success',
                      html:
                        'You have successfully requested a appointment, thanks.',
                      confirmButtonText: 'Okay, thanks!',
                      showCancelButton: false,
                      allowOutsideClick: false
                    }).then(function () {
                      location.reload();
                    }).catch(swal.noop);
                  } else {
                    swal({
                      title: 'Oops!',
                      type: 'error',
                      html:
                        "It didn't work, try again? (Error: " + res.message  + ")",
                      confirmButtonText: 'Lets try again!',
                      showCancelButton: true,
                      allowOutsideClick: false
                    }).then(function () {
                      openBooking();
                    }).catch(swal.noop);
                  }
                });
              }
            });
          }).catch(swal.noop);
        }, function () {
          swal.resetDefaults()
        });
    });
}

$('#bookings').click(function() {
  openBooking();
});

function redirectSwal(title, button_text, url) {
  swal({
    title: title,
    type: 'info',
    cancelButtonColor: '#d33',
    confirmButtonText: button_text,
    showCancelButton: false,
    allowOutsideClick: true
  }).then(function () {
    window.location = getUrl() + url;
  }).catch(swal.noop);
}

function reload() {
  location.reload();
}

function apiSwal(title, message, type, button_text, cancel_text, success_text, url) {
  swal({
    title: title,
    text: message,
    type: type,
    cancelButtonColor: '#d33',
    confirmButtonText: button_text,
    cancelButtonText: cancel_text,
    showCancelButton: false,
    allowOutsideClick: true
  }).then(function () {
    getData(location.origin + url, function(response) {
      if(response.response == 1) {
        swal({
          title: 'Error',
          text: response.message,
          type: 'error',
          confirmButtonText: 'Oops?',
          showCancelButton: false,
          allowOutsideClick: false
        }).catch(swal.noop);
      } else {
        var clicked = false;
        swal({
          title: 'Its done!',
          type: 'success',
          html: success_text,
          confirmButtonText: 'Okay',
          showCancelButton: false,
          allowOutsideClick: false
        }).then(function () {
          clicked = true;
          location.reload();
        }).catch(swal.noop);
        /*if(clicked != true) {
          setTimeout(reload, 5000);
        }*/
      }
    });

  }).catch(swal.noop);
}
