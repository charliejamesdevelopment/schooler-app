$('#create_ticket').click(function() {
  swal.setDefaults({
  input: 'text',
  confirmButtonText: 'Next &rarr;',
  showCancelButton: true,
  animation: false,
  progressSteps: ['1', '2', '3', '4', '5'],
  allowOutsideClick: false
})

var steps = [
  {
    title: 'Select Type',
    input: 'select',
    type: 'question',
    inputOptions: {
      "Appointment" : "Appointment Questions / Problems",
      "Teachers" : "Teachers Question / Problems",
      "Children" : "Children",
      "Other" : "Other"
    },
    inputPlaceholder: 'Select type...',
    showCancelButton: true,
    inputValidator: function (value) {
      return new Promise(function (resolve, reject) {
        if (value != '') {
          resolve()
        } else {
          reject('Make sure to select a type!')
        }
      })
    }
  },
  {
    title: 'Select Priority',
    input: 'select',
    type: 'question',
    inputOptions: {
      "Low" : "Low",
      "Medium" : "Medium",
      "High" : "High"
    },
    inputPlaceholder: 'Select priority...',
    showCancelButton: true,
    inputValidator: function (value) {
      return new Promise(function (resolve, reject) {
        if (value != '') {
          resolve()
        } else {
          reject('Make sure to select a ticket priority!')
        }
      })
    }
  },
  {
    title: 'Ticket Title',
    input: 'text',
    type: 'question',
    inputPlaceholder: 'Enter ticket title...',
    showCancelButton: true,
    inputValidator: function (value) {
      return new Promise(function (resolve, reject) {
        if (value != '') {
          resolve()
        } else {
          reject('Make sure to name your ticket!')
        }
      })
    }
  },
  {
    title: 'How can we help you?',
    input: 'textarea',
    type: 'question',
    inputPlaceholder: 'Whats the problem?',
    showCancelButton: true,
    inputValidator: function (value) {
      return new Promise(function (resolve, reject) {
        if (value != '') {
          resolve()
        } else {
          reject('Make sure to describe whats wrong!')
        }
      })
    }
  }
]

swal.queue(steps).then(function (result) {
  swal.resetDefaults()
  swal({
    title: 'Confirm, please.',
    html:
      '<pre>Type: <b>' + result[0] + "</b><br/>"+
      'Priority: <b>' + result[1] + "</b><br/>"+
      'Name: <b>' + result[2] + "</b><br/>" +
      'Description: <b>' + result[3] +"</b>"+
      '</pre>',
    confirmButtonText: 'Open Ticket',
    showCancelButton: true
  }).then(function () {
    var data = {"type" : result[0],
    "priority" : result[1],
    "name" : result[2],
    "description" : result[3],
    "author" : account_uuid,
    "username" : username}
    postData(getUrl() + "/api/create_ticket", data, function(res) {
      console.log(res);
      if(res.response == 0) {
        swal({
          title: 'Created!',
          type: 'success',
          html:
            '<pre>Type: <b>' + result[0] + "</b><br/>"+
            'Priority: <b>' + result[1] + "</b><br/>"+
            'Name: <b>' + result[2] + "</b><br/>" +
            'Description: <b>' + result[3] +"</b>"+
            '</pre>',
          confirmButtonText: 'Great!',
          showCancelButton: true
        }).then(function () {
          location.reload();
        });
      } else {
        swal({
          title: 'Oops!',
          type: 'error',
          html:
            "Error: " + res.message,
          confirmButtonText: 'Lets try again!',
          showCancelButton: true,
        }).then(function () {

        }).catch(swal.noop);
      }
    })
  });
}, function () {
  swal.resetDefaults()
})
});
