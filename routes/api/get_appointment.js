var express = require('express');
var router = express.Router();
var utils = require('../utils/utils');
var bcrypt = require('bcrypt-nodejs');
router.get('/get_appointment/:id',function(req,res){
    utils.getAppointmentData(req.params.id, function(call) {
      if(call == 1) {
        res.send({"response" : 1, "message" : "Invalid appointment!"});
      } else {
        res.send(call);
      }
    });
});

module.exports = router;
