var express = require('express');
var router = express.Router();
var utils = require('../utils/utils');
var bcrypt = require('bcrypt-nodejs');

router.get('/delete_student/:id',function(req,res){
    utils.getStudentData(req.params.id, function(user_data) {
      if(user_data == 1) {
        res.send({"response" : 1, "message" : "Invalid student!"});
      } else {
        utils.deleteStudentAdmin(req.params.id, function(call) {
          if(call == 1) {
            res.send({"response" : 1, "message" : "Something went wrong!"});
          } else {
            res.send({"response" : 0});
          }
        });
      }
    });
});

module.exports = router;
