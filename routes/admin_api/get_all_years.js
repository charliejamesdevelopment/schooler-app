var express = require('express');
var router = express.Router();
var utils = require('../utils/utils');

router.get('/get_all_years',function(req,res){
  utils.getAllYears(function(callback) {
    if(callback == 1) {
      res.send({"response" : 1, "message" : "No years are currently avaliable!"});
    } else {
      res.send(callback);
    }
  });
});

module.exports = router;
